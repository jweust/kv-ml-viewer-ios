//
//  LoadLabels.swift
//  KV-ML-Viewer
//
//  Created by Joshua Weust on 8/23/21.
//

import Foundation

extension PreviewViewController {
    
    /// Loads the labels from the labels file and stores them in the `labels` property.
    func loadLabels(labelsPath: String) -> [String]? {
        var labels = [String]()
        guard let fileURL = Bundle.main.url(forResource: labelsPath, withExtension: ".txt") else {
          print("Labels file not found in bundle. Please add a labels file with name " +
                       "\(labelsPath).txt and try again.")
            
            return nil
        }
        do {
          let contents = try String(contentsOf: fileURL, encoding: .utf8)
          labels = contents.components(separatedBy: .newlines)
        } catch {
          print("Labels file named \(labelsPath).txt cannot be read. Please add a " +
                       "valid labels file and try again.")
            return nil
        }
        
        return labels
    }
    
}
