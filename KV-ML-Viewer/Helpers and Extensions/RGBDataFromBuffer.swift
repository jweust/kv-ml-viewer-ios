//
//  RGBDataFromBuffer.swift
//  KV-ML-Viewer
//
//  Created by Joshua Weust on 8/17/21.
//

import Accelerate
import AVFoundation
import Foundation

extension PreviewViewController {
    func rgbDataFromBuffer(
        _ buffer: CVPixelBuffer,
        inputChannels: Int,
        inputSize: CGSize,
        isModelQuantized: Bool,
        redChannelMean: Double = 123.68,
        greenChannelMean: Double = 116.779,
        blueChannelMean: Double = 103.939
    ) -> Data? {
        CVPixelBufferLockBaseAddress(buffer, .readOnly)
        defer {
            CVPixelBufferUnlockBaseAddress(buffer, .readOnly)
        }
        guard let sourceData = CVPixelBufferGetBaseAddress(buffer) else {
            return nil
        }
        
        let width = CVPixelBufferGetWidth(buffer)
        let height = CVPixelBufferGetHeight(buffer)
        let sourceBytesPerRow = CVPixelBufferGetBytesPerRow(buffer)
        let destinationChannelCount = 3
        let destinationBytesPerRow = destinationChannelCount * width
        
        var sourceBuffer = vImage_Buffer(data: sourceData,
                                         height: vImagePixelCount(height),
                                         width: vImagePixelCount(width),
                                         rowBytes: sourceBytesPerRow)
        
        guard let destinationData = malloc(height * destinationBytesPerRow) else {
            print("Error: out of memory")
            return nil
        }
        
        defer {
            free(destinationData)
        }
        
        var destinationBuffer = vImage_Buffer(data: destinationData,
                                              height: vImagePixelCount(height),
                                              width: vImagePixelCount(width),
                                              rowBytes: destinationBytesPerRow)
        
        if (CVPixelBufferGetPixelFormatType(buffer) == kCVPixelFormatType_32BGRA) {
            vImageConvert_BGRA8888toRGB888(&sourceBuffer, &destinationBuffer, UInt32(kvImageNoFlags))
        } else if (CVPixelBufferGetPixelFormatType(buffer) == kCVPixelFormatType_32ARGB) {
            vImageConvert_ARGB8888toRGB888(&sourceBuffer, &destinationBuffer, UInt32(kvImageNoFlags))
        }
        
        let byteData = Data(bytes: destinationBuffer.data, count: destinationBuffer.rowBytes * height)
        if isModelQuantized {
            return byteData
        }
        
        // Not quantized, convert to floats
        let bytes = Array<UInt8>(unsafeData: byteData)!
        var floats = [Float]()
        
        for i in stride(from: 0, to: bytes.count, by: 3) {
                    
            let red = (Float(bytes[i]) - Float(redChannelMean))
            
            let green = (Float(bytes[i + 1]) - Float(greenChannelMean))
            
            let blue = (Float(bytes[i + 2]) - Float(blueChannelMean))
            
            floats.append(contentsOf: [red,green,blue])
            
        }
        
        return Data(copyingBufferOf: floats)
    }
}
