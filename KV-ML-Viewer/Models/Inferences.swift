//
//  Inferences.swift
//  KV-ML-Viewer
//
//  Created by Joshua Weust on 8/16/21.
//

import UIKit

struct BasicInference {
    let boundingBox: CGRect
    let confidence: Float
    let className: String
}
