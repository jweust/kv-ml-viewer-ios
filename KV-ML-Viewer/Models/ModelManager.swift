//
//  ModelFile.swift
//  KV-ML-Viewer
//
//  Created by Joshua Weust on 8/12/21.
//
import CoreGraphics
import TensorFlowLite

class ModelManager: CustomStringConvertible {
    var modelPath: String
    var modelExtension: String
    var labelMapPath: String?
    var labels: [String]?
    var requiredInputSize: CGSize
    var requiredInputChannels: Int
    var quantized: Bool
    var gpuEnabled: Bool
    var interpreter: Interpreter?
    
    init(modelPath: String, modelExtension: String, labelMapPath: String?, labels: [String]?, requiredInputSize: CGSize, requiredInputChannels: Int, quantized: Bool, gpuEnabled: Bool, interpreter: Interpreter?) {
        self.modelPath = modelPath
        self.modelExtension = modelExtension
        self.labelMapPath = labelMapPath ?? nil
        self.labels = labels ?? nil
        self.requiredInputSize = requiredInputSize
        self.requiredInputChannels = requiredInputChannels
        self.quantized = quantized
        self.gpuEnabled = gpuEnabled
        self.interpreter = interpreter ?? nil
    }
    
    var description: String {
        return "ModelManager(modelPath: \(modelPath), modelExtension: \(modelExtension), labelMapPath: \(String(describing: labelMapPath)), labels: \(String(describing: labels?.count)), requiredInputSize: \(requiredInputSize), requiredInputChannels: \(requiredInputChannels), quantized: \(quantized), gpuEnabled: \(gpuEnabled), interpreter: \(String(describing: interpreter))"
    }
}
