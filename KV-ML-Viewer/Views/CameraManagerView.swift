//
//  CameraManagerView.swift
//  KV-ML-Viewer
//
//  Created by Joshua Weust on 6/8/21.
//

import UIKit
import AVFoundation

class CameraManagerView: UIView {
    
    var delegate:CameraManagerDelegate?
    let videoOutput = AVCaptureVideoDataOutput()
    let photoOutput = AVCapturePhotoOutput()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        checkPermissions()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func checkPermissions() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            self.setupCaptureSession()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    DispatchQueue.main.async {
                        self.setupCaptureSession()
                    }
                } else {
                    print("The user has not yet granted access to the camera.")
                    
                }
            }
        case .denied:
            print("The user has denied camera access.")
        case .restricted:
            print("Camera access is restricted.")
        default:
            print("Unable to access the camera feed.")
        }
        
    }
    
    private func setupCaptureSession() {
        let captureSession = AVCaptureSession()
        captureSession.beginConfiguration()
        
        captureSession.sessionPreset = .high
        let queue = DispatchQueue(label: "videoOutput")
        videoOutput.setSampleBufferDelegate(self, queue: queue)
        videoOutput.videoSettings = [ String(kCVPixelBufferPixelFormatTypeKey) : kCMPixelFormat_32BGRA]
        videoOutput.alwaysDiscardsLateVideoFrames = true
        photoOutput.isHighResolutionCaptureEnabled = true
        
        if let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) {
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice)
                if captureSession.canAddInput(input) {
                    captureSession.addInput(input)
                }
            } catch let error {
                print("Failed to set input device with error: \(error)")
            }
            
            if captureSession.canAddOutput(videoOutput) {
                captureSession.addOutput(videoOutput)
            }
            
            if captureSession.canAddOutput(photoOutput) {
                captureSession.addOutput(photoOutput)
            }
            
            let cameraLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            cameraLayer.frame = self.frame
            cameraLayer.videoGravity = .resizeAspectFill
            self.layer.addSublayer(cameraLayer)
            
            captureSession.commitConfiguration()
            captureSession.startRunning()
        }
    }
}

extension CameraManagerView: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        connection.videoOrientation = .portrait
        
        let pixelBuffer: CVPixelBuffer? = CMSampleBufferGetImageBuffer(sampleBuffer)
        
        guard let imagePixelBuffer = pixelBuffer else {
            return
        }
        
        delegate?.didOutput(inputBuffer: imagePixelBuffer)    }
}

protocol CameraManagerDelegate {
    func didOutput(inputBuffer: CVPixelBuffer)
}
