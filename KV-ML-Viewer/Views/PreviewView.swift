//
//  PreviewView.swift
//  KV-ML-Viewer
//
//  Created by Joshua Weust on 8/12/21.
//

import UIKit

class PreviewView: UIView {
    var cameraManagerView: CameraManagerView!
    var captureButton: UIButton!
    var boundingBoxLayer: CALayer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Instantiate subviews and configure them
        setupSubviews(viewFrame: frame)
        
        // Layout subview constraints
        configureConstraints()
        
        // Prepare the bounding box layer
        prepareBoundingBoxLayer()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupSubviews(viewFrame: CGRect) {
        cameraManagerView = CameraManagerView(frame: viewFrame)
        cameraManagerView.translatesAutoresizingMaskIntoConstraints = false
        
        let mediumConfig = UIImage.SymbolConfiguration(pointSize: 60, weight: .semibold, scale: .medium)
        guard let cameraImage = UIImage(systemName: "camera.circle", withConfiguration: mediumConfig) else { return }
        captureButton = UIButton(type: .system)
        captureButton.tintColor = .lightGray
        captureButton.isOpaque = false
        captureButton.setImage(cameraImage, for: .normal)
        captureButton.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(cameraManagerView)
        addSubview(captureButton)
    }
    
    private func configureConstraints() {
        NSLayoutConstraint.activate([
            cameraManagerView.topAnchor.constraint(equalTo: topAnchor),
            cameraManagerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            cameraManagerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            cameraManagerView.heightAnchor.constraint(equalTo: heightAnchor),
            
            captureButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20),
            captureButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            captureButton.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.05)
        ])
    }
    
    // Prepares a CALayer for drawing of result bounding box
    private func prepareBoundingBoxLayer() {
        boundingBoxLayer = CALayer()
        boundingBoxLayer.name = "Bounding Box"
        boundingBoxLayer.bounds = CGRect(x: 0.0, y: 0.0, width: frame.width, height: frame.height)
        boundingBoxLayer.position = CGPoint(x: frame.midX, y: frame.midY)
        layer.addSublayer(boundingBoxLayer)
    }
}
