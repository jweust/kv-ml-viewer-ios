//
//  PreviewViewController.swift
//  KV-ML-Viewer
//
//  Created by Joshua Weust on 8/12/21.
//

import UIKit
import AVFoundation
import TensorFlowLite

class PreviewViewController: UIViewController, CameraManagerDelegate {
    private var previewView: PreviewView!
    private var photoOutput: AVCapturePhotoOutput!
    
    // Bool to determine if runModels() should be called after a buffer output
    private var shouldRunModels = false
    
    // Bool to determine if we should draw bounding boxes on preview from model results
    private var drawBoundingBoxes = false
    
    // Bool to determine if we should grab a frame to process for a still image
    private var captureStillImage = false
    
    // List of in-use models
    private var models = [ModelManager]()
    private var coreMLDelegate: CoreMLDelegate?
    
    // Generic threshold for result filtering
    private var generalConfidenceThreshold: Float = 0.6
    
    // Example model manager using a basic mobilenet model
    var mobileNetModel = ModelManager(modelPath: "ssd_mobilenet_v1_1_default_1", modelExtension: ".tflite", labelMapPath: "labelmap", labels: [String](), requiredInputSize: CGSize(width: 300, height: 300), requiredInputChannels: 3, quantized: false, gpuEnabled: false, interpreter: nil)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        previewView = PreviewView(frame: self.view.frame)
        // Set the camera manager delegate
        previewView.cameraManagerView.delegate = self
        self.view = previewView
        
        photoOutput = previewView.cameraManagerView.photoOutput
        previewView.captureButton.addTarget(self, action: #selector(capturePhoto), for: .touchUpInside)
        
        populateModelsList()
    }
    
    // MARK: -- Model Prep
    private func populateModelsList() {
    
        // Manually add each model to this list for preparation
        models.append(mobileNetModel)
        
        prepareModels()
    }
    
    private func prepareModels() {
        // If there are models to prepare
        if  models.count > 0 {
            for model in models {
                // Prepare the interpreters for the model
                guard let modelPath = Bundle.main.path(forResource: model.modelPath, ofType: model.modelExtension) else {
                    print("Failed to get model path for model \(model)")
                    return
                }
                do {
                    // If there are labels with the model, load them here
                    if model.labelMapPath != nil {
                        model.labels = loadLabels(labelsPath: model.labelMapPath!)
                    }
                    
                    // Prepare the interpreter. If model is able to use the GPU, use a CoreMLDelegate
                    if model.gpuEnabled {
                        var options = CoreMLDelegate.Options()
                        options.enabledDevices = .all
                        
                        coreMLDelegate = CoreMLDelegate(options: options)
                        
                        let interpreter = try Interpreter(modelPath: modelPath, delegates: [coreMLDelegate!])
                        model.interpreter = interpreter
                    } else {
                        let interpreter = try Interpreter(modelPath: modelPath)
                        model.interpreter = interpreter
                    }
                    
                    try model.interpreter!.allocateTensors()
                }
                catch let error {
                    print("Failed to create the interpreter(s) with error: \(error)")
                    return
                }
            }
        } else {
            print("Failed to prepare models for use: Models list is empty.")
            return
        }
        
        shouldRunModels = true
    }
    
    // MARK: -- Run interpreter on the video buffer
    private func runModelOnBuffer(modelToRun: ModelManager, pixelBuffer: CVPixelBuffer) -> [BasicInference]? {
        // Pause running models until we have processed the current frame at least
        shouldRunModels = false
        
        // Make sure that we set this back to true, no matter where we exit this function
        defer {
            shouldRunModels = true
        }
        
        guard let model = models.first(where: { $0.modelPath == modelToRun.modelPath}) else {
            print("Failed to run model: \(modelToRun). Unable to find matching model in prepared models list.")
            return nil
        }
        
        guard let scaledPixelBuffer = pixelBuffer.resized(to: model.requiredInputSize) else {
            print("Failed to properly scale the pixel buffer.")
            return nil
        }
        
        guard let interpreter = model.interpreter else {
            print("Failed to get interpreter for model: \(model)")
            return nil
        }
        
        let outputBoundingBox: Tensor
        let outputClasses: Tensor
        let outputScores: Tensor
        let outputCount: Tensor
        
        do {
            let inputTensor = try interpreter.input(at: 0)
            
            //Subtract channel means from image. Channel means: [123.68, 116.779, 103.939]
            guard let rgbBufferData = rgbDataFromBuffer(
                scaledPixelBuffer,
                inputChannels: model.requiredInputChannels,
                inputSize: model.requiredInputSize,
                isModelQuantized: inputTensor.dataType == .uInt8
                ) else {
                    print("Failed to convert the image buffer to RGB data.")
                    return nil
            }
            // Copy the RGB data to the input `Tensor`.
            try interpreter.copy(rgbBufferData, toInputAt: 0)
            try interpreter.invoke()
            
            //Detection model has four outputs: Boxes, Classes, Scores, Number of Boxes
            outputBoundingBox = try interpreter.output(at: 0)
            outputClasses = try interpreter.output(at: 1)
            outputScores = try interpreter.output(at: 2)
            outputCount = try interpreter.output(at: 3)
            
            let boundingBoxes = [Float](unsafeData: outputBoundingBox.data) ?? []
            let classes = [Float](unsafeData: outputClasses.data) ?? []
            let scores = [Float](unsafeData: outputScores.data) ?? []
            let count = Int(([Float](unsafeData: outputCount.data) ?? [0])[0])
            
            //Could just use the 'initialScaleSize' width and height values here, just being paranoid in case it scales improperly
            let scaledWidth = CGFloat(CVPixelBufferGetWidth(scaledPixelBuffer))
            let scaledHeight = CGFloat(CVPixelBufferGetHeight(scaledPixelBuffer))
            let results = formatResults(model: model, boundingBoxes: boundingBoxes, outputClasses: classes, outputScores: scores, outputCount: count, width: scaledWidth, height: scaledHeight)
            
            return results
        } catch let error{
            print("Failed to invoke the interpreter with error: \(error.localizedDescription)")
            return nil
        }
    }
    
    private func runModelsOnImageCapture(model: ModelManager, capturedImageBuffer: CVPixelBuffer) {
        captureStillImage = false
        
        guard let inferenceResults = runModelOnBuffer(modelToRun: model, pixelBuffer: capturedImageBuffer) else {
            print("Failed to get results from model: \(model) on imageCapture.")
            return
        }
        
        displayCaptureResults(imageBuffer: capturedImageBuffer, results: inferenceResults)
    }
    
    // Filters out all the results with confidence score < threshold and returns the top N results
    // sorted in descending order.
    private func formatResults(model: ModelManager, boundingBoxes: [Float], outputClasses: [Float], outputScores: [Float], outputCount: Int, width: CGFloat, height: CGFloat) -> [BasicInference] {
        var resultsArray: [BasicInference] = []
        if (outputCount == 0) {
            return resultsArray
        }
        for i in 0...outputCount - 1 {
            let score = outputScores[i]
            if score >= generalConfidenceThreshold {
                let outputClassIndex = Int(outputClasses[i])
                let outputClass = model.labels?[outputClassIndex + 1] ?? ""
                var rect: CGRect = CGRect.zero
                
                // Translates the detected bounding box to CGRect.
                rect.origin.y = CGFloat(boundingBoxes[4*i])
                rect.origin.x = CGFloat(boundingBoxes[4*i+1])
                rect.size.height = CGFloat(boundingBoxes[4*i+2]) - rect.origin.y
                rect.size.width = CGFloat(boundingBoxes[4*i+3]) - rect.origin.x
            
                let inference = BasicInference(boundingBox: rect, confidence: score, className: String(outputClass))
                resultsArray.append(inference)
            }
        }
        
        // Sort results in descending order of confidence.
        resultsArray.sort { (first, second) -> Bool in
            return first.confidence  > second.confidence
        }
        
        return resultsArray
    }
    
    private func displayResultsOnPreview(results: [BasicInference]) {
        // Display results to user on the video preview, if desired
        let sortedResults = results.sorted(by: {$0.confidence > $1.confidence})
        guard let bestResult = sortedResults.first else {
            print("Failed to get best result for displaying.")
            return
        }
        
        drawBoundingBoxOnPreview(boundingBox: bestResult.boundingBox, className: bestResult.className)
    }
    
    private func displayCaptureResults(imageBuffer: CVPixelBuffer, results: [BasicInference]) {
        // Pause inferencing while displaying results of capture
        shouldRunModels = false
        
        if results.count == 0 {
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: "No Results", message: "Nothing detected in the captured image. Please try capturing again.", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Try Again", style: .default, handler: {_ in
                    self.shouldRunModels = true
                }))
                self.present(alertController, animated: true)
            }
        } else {
            // Create an image to pass to the results view from the pixel buffer
            let ciImage = CIImage(cvPixelBuffer: imageBuffer)
            let resultsImage = UIImage(ciImage: ciImage)
            
            DispatchQueue.main.async {
                // Instantiate the results view with the necessary data
                let resultsViewController = ResultsViewController(frame: self.view.frame, capturedImage: resultsImage, results: results)
                
                // Present the results view, making sure to switch shouldRunModels back to true when it is closed
                self.navigationController?.pushViewController(resultsViewController, animated: true)
            }
        }
        self.shouldRunModels = true
    }
    
    private func drawBoundingBoxOnPreview(boundingBox: CGRect, className: String) {
        // Clear out old bounding box
        previewView.boundingBoxLayer.sublayers = nil
        
        DispatchQueue.main.async {
            let scaledBox = boundingBox.applying(CGAffineTransform(scaleX: self.view.frame.width, y: self.view.frame.height))
            
            let shapeLayer = CAShapeLayer()
            shapeLayer.bounds = scaledBox
            shapeLayer.position = CGPoint(x: scaledBox.midX, y: scaledBox.midY)
            shapeLayer.borderColor = UIColor.red.cgColor
            shapeLayer.borderWidth = 2
            shapeLayer.name = className
            
            self.previewView.boundingBoxLayer.addSublayer(shapeLayer)
        }
    }
}

extension PreviewViewController: AVCapturePhotoCaptureDelegate {
    @objc func capturePhoto() {
        print("Capturing photo")
        captureStillImage = true
        
    }
    
    
    // MARK: CameraManagerDelegate Methods
    func didOutput(inputBuffer: CVPixelBuffer) {
        if captureStillImage {
            runModelsOnImageCapture(model: mobileNetModel, capturedImageBuffer: inputBuffer)
            
            let captureSoundId = SystemSoundID(1108)
            AudioServicesPlaySystemSound(captureSoundId)
        }
        if shouldRunModels {
            // Running single model here, could also loop through all models in list to run multiple models
            let modelResults = runModelOnBuffer(modelToRun: mobileNetModel, pixelBuffer: inputBuffer)
            
            // Setting this to true for example, can be modified anywhere in scope to disable live drawing
            drawBoundingBoxes = true
            
            if modelResults!.count > 0 && drawBoundingBoxes {
                displayResultsOnPreview(results: modelResults!)
            }
        }
    }
}
