//
//  ResultsView.swift
//  KV-ML-Viewer
//
//  Created by Joshua Weust on 8/25/21.
//

import UIKit

class ResultsView: UIView {
    var stackView: UIStackView!
    var buttonsStackView: UIStackView!
    var imageView: UIImageView!
    var resultsLabel: UILabel!
    var cancelButton: UIButton!
    var uploadButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .systemBackground
        
        // Instantiate subviews and configure them
        setupSubviews(viewFrame: frame)
        
        // Layout subview constraints
        configureConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupSubviews(viewFrame: CGRect) {
        stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        
        imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        
        resultsLabel = UILabel()
        resultsLabel.translatesAutoresizingMaskIntoConstraints = false
        resultsLabel.font = .systemFont(ofSize: 14)
        resultsLabel.lineBreakMode = .byWordWrapping
        resultsLabel.numberOfLines = 3
        
        buttonsStackView = UIStackView()
        buttonsStackView.translatesAutoresizingMaskIntoConstraints = false
        buttonsStackView.alignment = .center
        buttonsStackView.axis = .horizontal
        buttonsStackView.distribution = .fillEqually
        
        cancelButton = UIButton(type: .system)
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.titleLabel!.font = .systemFont(ofSize: 24)
        
        uploadButton = UIButton(type: .system)
        uploadButton.translatesAutoresizingMaskIntoConstraints = false
        uploadButton.titleLabel!.font = .systemFont(ofSize: 24)
        
        buttonsStackView.addArrangedSubview(cancelButton)
        buttonsStackView.addArrangedSubview(uploadButton)
        
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(resultsLabel)
        stackView.addArrangedSubview(buttonsStackView)
        addSubview(stackView)
    }
    
    func configureConstraints() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 100),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            resultsLabel.heightAnchor.constraint(equalToConstant: 40),
            
            buttonsStackView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor),
            buttonsStackView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor),
            
            imageView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.75),
            imageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.75)
        ])
    }
}
