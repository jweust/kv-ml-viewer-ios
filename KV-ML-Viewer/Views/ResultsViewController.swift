//
//  ResultsViewController.swift
//  KV-ML-Viewer
//
//  Created by Joshua Weust on 8/25/21.
//

import UIKit
import CoreGraphics

class ResultsViewController: UIViewController {
    private var resultsView: ResultsView!
    private var viewFrame: CGRect
    
    private var capturedImage: UIImage
    private var results: [BasicInference]
    
    private var resultsDescription = ""

    init(frame: CGRect, capturedImage: UIImage, results: [BasicInference]) {
        self.viewFrame = frame
        self.capturedImage = capturedImage
        self.results = results
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Upload Image"
        
        resultsView = ResultsView(frame: viewFrame)
        self.view = resultsView

        // Do any additional setup after loading the view.
        prepareImage()
        formatResultsDescription()
        configureButtons()
    }
    
    private func prepareImage() {
        let preparedImage = drawResultsOnImage()
        
        DispatchQueue.main.async {
            self.resultsView.imageView.image = preparedImage
        }
    }
    
    private func formatResultsDescription() {
        for i in 0..<results.count {
            let formattedConfidence = results[i].confidence * 100
            resultsDescription += "\(i+1): \(results[i].className) - \(formattedConfidence)%"
            
            if i < results.count {
                resultsDescription += ", "
            }
        }
        
        resultsView.resultsLabel.text = resultsDescription
    }
    
    private func configureButtons() {
        resultsView.cancelButton.setTitle("Cancel", for: .normal)
        resultsView.cancelButton.addTarget(self, action: #selector(cancelClicked), for: .touchUpInside)
        
        resultsView.uploadButton.setTitle("Upload", for: .normal)
        resultsView.uploadButton.addTarget(self, action: #selector(uploadClicked), for: .touchUpInside)
    }
    
    @objc func cancelClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func uploadClicked() {
        print("Upload")
    }
    
    private func sendResultsToServer() {
        // Send results off to backend
    }

    private func drawResultsOnImage() -> UIImage {
        let imageSize = capturedImage.size
        let scale: CGFloat = 0
        
        UIGraphicsBeginImageContextWithOptions(imageSize, false, scale)

        print("Result size: \(results.count)")
        capturedImage.draw(at: CGPoint.zero)
        for i in 0..<results.count {
            let result = results[i]
            
            let newRect = result.boundingBox.applying(CGAffineTransform(scaleX: imageSize.width, y: imageSize.height))
            UIGraphicsGetCurrentContext()?.setLineWidth(10.0)
            UIColor.red.setStroke()
            UIRectFrame(newRect)
            
            let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 20), .foregroundColor: UIColor.white]
            let string = "\(i+1)"
            let attributedString = NSAttributedString(string: string, attributes: attrs)
            attributedString.draw(with: CGRect(x: newRect.maxX-25, y: newRect.maxY-35, width: 30, height: 20), options: .usesLineFragmentOrigin, context: nil)
        }

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
